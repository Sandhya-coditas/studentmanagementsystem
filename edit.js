const form = document.createElement("form");
const editButton = document.getElementById("edit");

edit.addEventListener("click", function editForm() {

    form.setAttribute("method", "put");
    form.setAttribute("action", "http://cc83-103-51-153-190.ngrok.io/updateStudent");

    const enrollmentNumber = document.createElement("input");
    enrollmentNumber.setAttribute("type", "number");
    enrollmentNumber.setAttribute("value", "enrollmentNumber");
    enrollmentNumber.setAttribute("placeholder", "EnrollmentNumber");
    enrollmentNumber.setAttribute("id", "enrollmentNumber");

    const firstName = document.createElement("input");
    firstName.setAttribute("type", "text");
    firstName.setAttribute("name", "firstName");
    firstName.setAttribute("placeholder", "FirstName");
    firstName.setAttribute("id", "firstName");

    const lastName = document.createElement("input");
    lastName.setAttribute("type", "text");
    lastName.setAttribute("name", "lastName");
    lastName.setAttribute("placeholder", "Last Name");
    lastName.setAttribute("id", "lastName");


    const gender = document.createElement("input");
    gender.setAttribute("type", "text");
    gender.setAttribute("name", "gender");
    gender.setAttribute("placeholder", "Gender");
    gender.setAttribute("id", "gender");


    const stream = document.createElement("input");
    stream.setAttribute("type", "text");
    stream.setAttribute("name", "stream");
    stream.setAttribute("placeholder", "Stream");
    stream.setAttribute("id", "stream");


    const rating = document.createElement("input");
    rating.setAttribute("type", "number");
    rating.setAttribute("value", "rating");
    rating.setAttribute("placeholder", "Rating");
    rating.setAttribute("id", "rating");

    const dob = document.createElement("dob");
    dob.setAttribute("type", "datetime");
    dob.setAttribute("value", "dob");
    dob.setAttribute("placeholder", "D.O.B");
    dob.setAttribute("id", "dob");

    const submit = document.createElement("input");
    submit.setAttribute("type", "submit");
    submit.setAttribute("value", "Submit");

    form.appendChild(enrollmentNumber);
    form.appendChild(firstName);
    form.appendChild(lastName);
    form.appendChild(gender);
    form.appendChild(stream);
    form.appendChild(rating);
    form.appendChild(dob);
    form.appendChild(submit);


    document.body.appendChild(form);



    if (form.addEventListener("submit", async (e) => {

        e.preventDefault();
        const eno = document.getElementById("enrollmentNumber").value;
        const fname = document.getElementById("firstName").value;
        const lname = document.getElementById("lastName").value;
        const sex = document.getElementById("gender").value;
        const str = document.getElementById("stream").value;
        const rate = document.getElementById("rating").value;
        const birth = document.getElementById("dob").value;

        try {
            const editRecord = await fetch("http://cc83-103-51-153-190.ngrok.io/updateStudent", {
                method: "PUT",
                headers: new Headers({
                    "ngrok-skip-browser-warning": "3200",
                    'contentType': 'application/json',
                    charset: 'utf-8',
                }),
                body: JSON.stringify({
                    enrollmentNumber: `${eno}`,
                    firstName: `${fname}`,
                    lastName: `${lname}`,
                    gender: `${sex}`,
                    stream: `${str}`,
                    rating: `${rate}`,
                    dob: `${birth}`

                }),
            });
        }
        catch (e) {
            console.log(e);
        }
    }
}
)







