const tHead = ["Enrollment Number", "First Name", "Last Name", "Gender", "Stream", "Rating", "D.O.B"];
const enrollmentNumber = document.getElementById('enrollmentNumber');
const firstName = document.getElementById('firstName');
const lastName = document.getElementById('lastName');
const gender = document.getElementById('gender');
const stream = document.getElementById('stream');
const rating = document.getElementById('rating');
const dob = document.getElementById('dob');


const createTable = () => {
    const table = document.createElement('table');
    table.className = 'table';

    const tableHead = document.createElement('thead');
    tableHead.className = 'tableHead';

    const tableHeadRow = document.createElement('tr');
    tableHeadRow.className = 'tableHeadRow';

    tHead.forEach(header => {
        const currentHeader = document.createElement('th');
        currentHeader.innerText = header;
        tableHeadRow.append(currentHeader);
    });
    tableHead.append(tableHeadRow);
    table.append(tableHead);

    const tableBody = document.createElement('tbody');
    tableBody.className = "table-Body";
    table.append(tableBody);
    document.body.append(table);


    const getStudents = async () => {
        try {
            let response = await fetch("http://cc83-103-51-153-190.ngrok.io/fetchAllStudent", {
                method: "get",
                headers: new Headers({
                    "ngrok-skip-browser-warning": "3200",
                })
            });
            let data = await response.json();
            console.log(data);
            result(data);
        }
        catch (error) {
            console.log(error);
        }

    }
    getStudents();


    function result(data) {

        let template = "";
        const placeholder = document.getElementById("tbody")

        for (let index = 0; index < data.length; index++) {
            template +=
                `<h1>STUDENT MANAGEMENT SYSTEM</h1>
                      <tr>
                         <td> ${data[index].enrollmentNumber} </td>
                         <td> ${data[index].firstName} </td>
                         <td> ${data[index].lastName} </td>
                         <td> ${data[index].gender} </td>
                         <td> ${data[index].stream} </td>
                         <td> ${data[index].rating} </td>
                         <td> ${data[index].dob} </td>
                         <td><button class="editButton">Edit</button>
                         <button class="deleteButton">Delete</button></td>

                    </tr>
                         `
        }
        console.log(template);
        placeholder.innerHTML = template;
    }

}
createTable();